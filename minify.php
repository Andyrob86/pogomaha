<?php
include "vendor/autoload.php";
$cssa = array();
$jsa = array();
$cssdir    = 'public/assets/css';
$jsdir    = 'public/assets/js';
if (isset($argv[1]) && $argv[1] === "update"){
	echo "Updating naming token of minified files!\r\n";
	$factory = new RandomLib\Factory;
	$generator = $factory->getLowStrengthGenerator();
	$token = $generator->generateString(10, '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ');
	$files = scandir($cssdir);
	foreach($files as $key => $file){
		if(substr($file,-10) == "-minme.css"){
			$cssa[] = str_replace("-minme.css","",$file);
		}
		if(substr($file,10) == ".token"){
			$oldtoken = substr($file,0,10);
		}
	}
	$files = scandir($jsdir);
	foreach($files as $key => $file){
		if(substr($file,-9) == "-minme.js"){
			$jsa[] = str_replace("-minme.js","",$file);
		}
	}
	$header = file_get_contents("public/header.php");
	$footer = file_get_contents("public/footer.php");
	$map = file_get_contents("public/map2.php");
	file_put_contents("public/header.php",str_replace($oldtoken,$token,$header));
	file_put_contents("public/footer.php",str_replace($oldtoken,$token,$footer));
	file_put_contents("public/map2.php",str_replace($oldtoken,$token,$map));
	
} else {
	$files = scandir($cssdir);
	foreach($files as $key => $file){
		if(substr($file,-10) == "-minme.css"){
			$cssa[] = str_replace("-minme.css","",$file);
		}
		if(substr($file,10) == ".token"){
			$token = substr($file,0,10);
		}
	}
	$files = scandir($jsdir);
	foreach($files as $key => $file){
		if(substr($file,-9) == "-minme.js"){
			$jsa[] = str_replace("-minme.js","",$file);
		}
	}
}
use MatthiasMullie\Minify;
$strlmax = strlen("Created file {$cssdir}111111111/css{$token}.min.php");
$i = 0;
function print_min($f,$t,$m){
	$eom = "..[DONE]\r\n";
	$padding = "";
	$rl = $m - strlen("Minifying {$f}.{$t}");
	while($rl>=0){
		$padding.=".";
		$rl--;
	}
	echo "Minifying {$f}.{$t}{$padding}{$eom}";
}

function print_done($t,$m, $token){
	$eom = "..[DONE]\r\n";
	$padding = "";
	$rl = $m - strlen("Created file {$token}.min.css");
	while($rl>=0){
		$padding.=".";
		$rl--;
	}
	if ($t == "css"){
		echo "Created file {$token}.min.css{$padding}{$eom}";
	}
	if ($t == "js"){
		echo "Created file {$token}.min.js...{$padding}{$eom}";
	}
}

foreach($cssa as $css){
	if ($strlmax < strlen("Minifying {$css}-minme.css")) {
		$strlmax = strlen("Minifying {$css}-minme.css");
	}
}
foreach($jsa as $js){
	if ($strlmax < strlen("Minifying {$js}-minme.js")) {
		$strlmax = strlen("Minifying {$js}-minme.js");
	}
}
foreach($cssa as $css){
	$minifier = new Minify\CSS("{$cssdir}/{$css}-minme.css");
	$minifier->add("{$cssdir}/{$css}-minme.css");
	print_min($css,"css",$strlmax);
	$minifier->minify("{$cssdir}/{$css}.{$token}.min.css");
	if(isset($argv[1]) && $argv[1] === "update")print_done("css",$strlmax, $css.$token);
}

foreach($jsa as $js){
	$minifier = new Minify\JS("{$jsdir}/{$js}-minme.js");
	$minifier->add("{$jsdir}/{$js}-minme.js");
	print_min($js,"js",$strlmax);
	$minifier->minify("{$jsdir}/{$js}.{$token}.min.js");
	if(isset($argv[1]) && $argv[1] === "update")print_done("js",$strlmax, $js.$token);
}
if (isset($argv[1]) && $argv[1] == "update") {
	foreach($cssa as $css){
		unlink("{$cssdir}/{$css}.{$oldtoken}.min.css");
	}
		foreach($jsa as $js){
		unlink("{$jsdir}/{$js}.{$oldtoken}.min.js");
	}
	
	unlink("{$cssdir}/{$oldtoken}.token");
	$tokenFile = fopen("{$cssdir}/{$token}.token", "w") or die("Unable to open file!");
	$txt = "Just a placeholder for the token number!";
	fwrite($tokenFile, $txt);
	fclose($tokenFile);

}

echo (isset($argv[1]) && $argv[1] == "update")?"Minifying complete - header.php, footer.php, and map2.php old token ({$oldtoken}) has been replaced with the new token: {$token}":"";

?>