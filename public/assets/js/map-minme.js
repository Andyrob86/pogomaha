$( document ).ready(function() {
	var dabestis = new x228(function() { dabest();});
	var wo = Cookies.getJSON('welcomeOpts');
	if(wo == null || wo.hide9_27_16 != 1 || wo.hide9_27_16 == null){
		$('#welcome_modal').modal('show');
	};
	var pokemon = (function () {
    var pokemon = null;
    $.ajax({
        'async': false,
        'global': false,
        'url': 'assets/js/pokemon.json',
        'dataType': "json",
        'success': function (data) {
            pokemon = data;
        }
    });
    return pokemon.pokemon;
	})(); 
});


$("#getHelp").click(function(){
	$("#help_modal").modal("show");
});

$("#controls-show").click(function(){
	$("#controls-show").toggle('slide', {direction:"right"},500, function(){
		$("#controls").toggle('slide',{direction:"right"}, 500);
	});
});

$("#controls-hide").click(function(){
	$("#controls").toggle('slide',{direction:"right"}, 500, function(){
		$("#controls-show").toggle('slide',{direction:"right"}, 500);
	});
});


$(document).on('click', function(event) {
  if (!$(event.target).closest('#controls').length) {
    if($("#controls").is(":visible")){
			$("#controls").toggle('slide',{direction:"right"}, 500, function(){
				$("#controls-show").toggle('slide',{direction:"right"}, 500);
			});
		}
  }
});


function checkHideWelcome(){
	if($("input#hideWelcome").is(":checked")){
		Cookies.set('welcomeOpts', {hide9_27_16: 1}, { expires: 90 });
	}
}

function createPokeSelectById(){
	
}

function cycleImages(){
      var $active = $('#nest_images .active');
      var $next = ($active.next().length > 0) ? $active.next() : $('#nest_images img:first');
      $next.css('z-index',2);//move the next image up the pile
      $active.fadeOut(1500,function(){//fade out the top image
	  $active.css('z-index',1).show().removeClass('active');//reset the z-index and unhide the image
          $next.css('z-index',3).addClass('active');//make the next image the top one
      });
    }

function getGeoLoc(){
	GMaps.geocode({
		address: $('#address').val(),
		callback: function(results, status) {
			if (status == 'OK') {
				var latlng = results[0].geometry.location;
				map.setCenter(latlng.lat(), latlng.lng());
				
			}
		}
	});
}

function geoCenter(){
	GMaps.geolocate({
		success: function(position){
			map.setCenter(position.coords.latitude, position.coords.longitude);
		},
		error: function(error){
			alert('Location Lookup failed: ' + error.message);
		},
		not_supported: function(){
			alert("Your browser does not support geolocation");
		}
	});
}

function getRoute(toLat,toLng,fromLat,fromLng){
	if (null == fromLat || null == fromLng){
	};
	map.drawRoute({
		origin: [41.247443, -96.100705],
		destination: [41.274967, -95.958469],
		travelMode: 'driving',
		strokeColor: '#131540',
		strokeOpacity: 0.6,
		strokeWeight: 6
	});
}
	
function suggestModal(id){
	$("#suggestForm").show();
	$("#suggestion_nestId").val(id);
	$("#suggestResponse").html("");
	$("#suggest_modal").modal('show');
}

function toggleHeatmap() {
	heatmap.setMap(heatmap.getMap() ? null : map.createHeatmap());
}


function changeGradient() {
	var gradient = [
		'rgba(0, 255, 255, 0)',
		'rgba(0, 255, 255, 1)',
		'rgba(0, 191, 255, 1)',
		'rgba(0, 127, 255, 1)',
		'rgba(0, 63, 255, 1)',
		'rgba(0, 0, 255, 1)',
		'rgba(0, 0, 223, 1)',
		'rgba(0, 0, 191, 1)',
		'rgba(0, 0, 159, 1)',
		'rgba(0, 0, 127, 1)',
		'rgba(63, 0, 91, 1)',
		'rgba(127, 0, 63, 1)',
		'rgba(191, 0, 31, 1)',
		'rgba(255, 0, 0, 1)'
	]
	heatmap.set('gradient', heatmap.get('gradient') ? null : gradient);
}

function changeRadius() {
	heatmap.set('radius', heatmap.get('radius') ? null : 20);
}

function changeOpacity() {
	heatmap.set('opacity', heatmap.get('opacity') ? null : 0.2);
}

      


function initMap(){
	var realMarkers = {};
	
	var map = new GMaps({
		div: '#map',
		zoom: 12,
		lat: 41.253541, 
		lng: -95.998511,
		scaleControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.RIGHT_BOTTOM
		},
		mapTypeControl: false,
		zoomControl: false,
		streetViewControl: false,
		
	});
	var pogoStyle = [{"featureType":"landscape.man_made","elementType":"geometry.fill","stylers":[{"color":"#a1f199"}]},{"featureType":"landscape.natural.landcover","elementType":"geometry.fill","stylers":[{"color":"#37bda2"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"color":"#37bda2"}]},{"featureType":"poi.attraction","elementType":"geometry.fill","stylers":[{"visibility":"on"}]},{"featureType":"poi.business","elementType":"geometry.fill","stylers":[{"color":"#e4dfd9"}]},{"featureType":"poi.business","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#37bda2"}]},{"featureType":"road","elementType":"geometry.fill","stylers":[{"color":"#84b09e"}]},{"featureType":"road","elementType":"geometry.stroke","stylers":[{"color":"#fafeb8"},{"weight":"1.25"}]},{"featureType":"road.highway","elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#5ddad6"}]}];
	var midnightStyle = [{"featureType":"all","elementType":"labels.text.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"all","elementType":"labels.text.stroke","stylers":[{"color":"#000000"},{"lightness":13}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#144b53"},{"lightness":14},{"weight":1.4}]},{"featureType":"landscape","elementType":"all","stylers":[{"color":"#08304b"}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#0c4152"},{"lightness":5}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#0b434f"},{"lightness":25}]},{"featureType":"road.arterial","elementType":"geometry.fill","stylers":[{"color":"#000000"}]},{"featureType":"road.arterial","elementType":"geometry.stroke","stylers":[{"color":"#0b3d51"},{"lightness":16}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#000000"}]},{"featureType":"transit","elementType":"all","stylers":[{"color":"#146474"}]},{"featureType":"water","elementType":"all","stylers":[{"color":"#021019"}]}];
	var vivid = [{"featureType":"landscape","elementType":"geometry.fill","stylers":[{"color":"#e9e5dc"}]},{"featureType":"landscape.natural.terrain","elementType":"geometry.fill","stylers":[{"color":"#44a04b"}]},{"featureType":"poi.park","elementType":"geometry.fill","stylers":[{"color":"#7bb718"}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#a3a2a2"}]},{"featureType":"water","elementType":"geometry.fill","stylers":[{"color":"#0099dd"}]}];
	map.addStyle({
			styledMapName:"Pokemon Go Map",
			styles: pogoStyle,
			mapTypeId: "pogo"  
	});
	map.addStyle({
			styledMapName:"Midnight Commander",
			styles: midnightStyle,
			mapTypeId: "midnight"  
	});
		map.addStyle({
			styledMapName:"Midnight Commander",
			styles: vivid,
			mapTypeId: "vivid"  
	});
	map.setStyle('pogo');
	
	
	map.addControl({
		position: 'LEFT_TOP',
		content: "<a href='http://pogomaha.com'><img src='assets/img/web/pokemongomaha100x47.png' /></a>",
		style: {
			margin: '5px',
			padding: '1px 6px',
		}
	});
	
	map.addControl({
		position: 'top_right',
		content: '<h4 class="text-center">Geolocate</h4',
		style: {
			margin: '5px',
			padding: '1px 6px',
			border: 'solid 1px #717B87',
			background: '#fff',
			width: '113px',
		},
		events: {
			click: function(){
				geoCenter();
			}
		}
	});
	
	$(".markersToggle input[type='checkbox']").click(function() {
		var poi_type = $(this).attr('name');
		if (poi_type != 'undefined'){
			if ($(this).is(':checked')){
				map.addMarkersOfType(poi_type);
			} else {
				map.removeMarkersOfType(poi_type);
			}
		}
	});
	
	$(".stylesToggle label").click(function() {
		$(".stylesToggle label").not(this).removeClass("active");
		var style_type = $(this).attr('name');
		if (style_type != 'undefined'){
			map.setStyle(style_type);
		}
	});
	
	$.ajax({
	  xhr: function(){
			var xhr = new window.XMLHttpRequest();
			//Upload progress
			xhr.upload.addEventListener("progress", function(evt){
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;

					//console.log("up:" + percentComplete);
				}
			}, false);
			//Download progress
			xhr.addEventListener("progress", function(evt){
				if (evt.lengthComputable) {
					var percentComplete = evt.loaded / evt.total;
					$("#downloadProgressBar").attr('aria-valuenow', Math.round((percentComplete * 100)));
					$("#downloadProgressBar").css('width',Math.round((percentComplete * 100))+"%");
					$('#percentComplete').animateNumber({ number: Math.round((percentComplete * 100)) },'normal',function(){$(".progress-bar").html("Download Complete!")});
					/*
					
					$("#downloadProgressBar").html(Math.round((percentComplete * 100))+"%");
					*/
				} else {
					
				}
			}, false);
			return xhr;
		},
		type: "POST",
		url: 'maphandler.php',
		data: {"data":1},
		dataType: "json",
		beforeSend: function(){

			$('#percentComplete').animateNumber({ number: 20 });
			$("#downloadProgressBar").attr('aria-valuenow', 20);
			$("#downloadProgressBar").css('width',"20%");
			
			/*
			$("#downloadProgressBar").attr('aria-valuenow', 20);
			$("#downloadProgressBar").css('width',"20%");
			
			*/
		},
		success: function(data){
			if (data != ""){
				var locations = data;

				GMaps.prototype.addMarkersOfType = function (poi_type) {
						// clear markers of this type
						realMarkers[poi_type]=[];
						// for each Gmaps marker
						for(var i=0; i<locations[poi_type].length; i++){
								// add the marker
								locations[poi_type][i].icon.size = new google.maps.Size(locations[poi_type][i].sx,locations[poi_type][i].sx);
								locations[poi_type][i].icon.origin = new google.maps.Point(locations[poi_type][i].ox,locations[poi_type][i].oy);
								locations[poi_type][i].icon.anchor = new google.maps.Point(locations[poi_type][i].ax,locations[poi_type][i].ay);
												
								var marker = map.addMarker(locations[poi_type][i]);
								// save it as real marker
								realMarkers[poi_type].push(marker);
						}
				}
				
				GMaps.prototype.removeMarkersOfType = function (poi_type) {
						// for each real marker of this type
						for(var i=0; i<locations[poi_type].length; i++){
								// remove the marker
								realMarkers[poi_type][i].setMap(null);
						}
						// clear markers of this type
						realMarkers[poi_type]=[];
				}
				/*
				GMaps.prototype.addPokeHeatMarkers = function () {
					var mon = $("#heat_mon").val();
						// clear markers of this type
						realMarkers[mon]=[];
						// for each Gmaps marker
						for(var i=0; i<locations['pokemons'][mon].length; i++){
								// add the marker
								locations['pokemons'][mon][i].icon.size = new google.maps.Size(locations['pokemons'][mon][i].sx,locations[mon][i].sx);
								locations['pokemons'][mon][i].icon.origin = new google.maps.Point(locations['pokemons'][mon][i].ox,locations[mon][i].oy);
								locations['pokemons'][mon][i].icon.anchor = new google.maps.Point(locations['pokemons'][mon][i].ax,locations[mon][i].ay);
												
								var marker = map.addMarker(locations['pokemons'][mon][i]);
								// save it as real marker
								realMarkers[mon].push(marker);
						}
				}
				

				GMaps.prototype.removePokeHeatMarkers = function (mon) {
						// for each real marker of this type
						for(var i=0; i<locations['pokemons'][mon].length; i++){
								// remove the marker
								realMarkers['pokemons'][mon][i].setMap(null);
						}
						// clear markers of this type
						realMarkers['pokemons'][mon]=[];
				}
				 */
				GMaps.prototype.getPoints = function () {
					var points = [];
					for (var i=0; i<locations['heatmap'].length; i++){
						points[i] = new google.maps.LatLng(locations['heatmap'][i]['lat'],locations['heatmap'][i]['lng']);
					}
					
					
					return points;
				}
				
				GMaps.prototype.getPokemonPoints = function (mon) {
					var points = [];
					var mon = $("#heat_mon").val();
					for (var i=0; i<locations['pokemons'][mon].length; i++){
						points[i] = new google.maps.LatLng(locations['pokemons'][mon][i]['lat'],locations['pokemons'][mon][i]['lng']);
					}
					
					
					return points;
				}
				
				GMaps.prototype.createHeatmap = function () {
					heatmap = new google.maps.visualization.HeatmapLayer({
						data: map.getPoints(),
						map: map.map,
						radius: 30,
						opacity:0.6
					});
				}
				
				GMaps.prototype.createPokemonHeatmap = function () {
					var mon = $("#heat_mon").val();
					pokemonHeatmap = new google.maps.visualization.HeatmapLayer({
						data: map.getPokemonPoints(mon),
						map: map.map,
						radius: 45,
						opacity:0.6
					});
				}
				
				
				//map.createHeatmap();
				//map.createPokemonHeatmap(123);
				map.addMarkersOfType('nests');
				setInterval('cycleImages()', 7000);
			}
		},
		complete: function(){
			setTimeout(function(){
				$("#progressWindow").hide(300);
			}, 5000);
			
		}
	});
	
	$("#toggleHeatmap").click(function(){
		if(typeof heatmap != 'undefined'){
			heatmap.setMap(heatmap.getMap() ? null : map.map);
		} else {
			map.createHeatmap(function(){
				heatmap.setMap(heatmap.getMap() ? null : map.map);
			});
			
		}
	});
 
	$("#togglePokeHeatmap").click(function(){
		

		if (!$('input#togglePokeHeatmap').is(':checked')) {
			pokemonHeatmap.setMap(null);
		} else {
			map.createPokemonHeatmap();
			//map.addPokeHeatMarkers();
		}


		
	});
	
	$("#heat_mon").on("change",function(){
		if ($('input#togglePokeHeatmap').is(':checked')) {
			pokemonHeatmap.setMap(null);
			map.createPokemonHeatmap();
			//map.addPokeHeatMarkers();
		}
	})
 
	$("#submitSuggestion").click(function(){
		var userIp = $("#userIp").val();
		var nestId = $("#suggestion_nestId").val();
		var mon = $("#suggestion_mon").val();
		$.ajax({
			 type: "POST",
			 url: 'maphandler.php',
			 data: {"suggestion":1,"userIp":userIp,"mon":mon,"nestId":nestId},
			 success: function(data) { 
				data = $.parseJSON(data);
				if (data.state == "success"){
					$("#suggestResponse").html(data.message);
					$("#suggestForm").hide();
				}
			 }
		});
	});
	
	
	// getRoute(1,2);

}