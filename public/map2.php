<!DOCTYPE html>
<html>
  <head>
		<title>PoGomaha | Map</title>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta charset="utf-8">
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
    <link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
    <link rel="manifest" href="/manifest.json">
    <meta name="theme-color" content="#ffffff">	
		<link rel="stylesheet" href="assets/font-awesome-4.6.3/css/font-awesome.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
		<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
		<link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
		

		<?php if ($_SERVER['SERVER_NAME'] != "localhost" || isset($_GET['min'])){ ?>
			<link href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/custom.KsbTKwvcVd.min.css" rel=stylesheet>
			<link rel="stylesheet" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/map.KsbTKwvcVd.min.css">
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/js.cookie.KsbTKwvcVd.min.js"></script>
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/283243.KsbTKwvcVd.min.js"></script>
		<?php } else { ?>
			<link href="/pogomaha/public/assets/css/custom-minme.css" rel=stylesheet>
			<link rel="stylesheet" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/map-minme.css">
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/js.cookie-minme.js"></script>
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/283243-minme.js"></script>
		<?php } ?>
		<link href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/bootstrap.min.css" rel=stylesheet>
		<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/bootstrap.min.js'></script>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
				margin: 0;
        padding: 0;
      }
    </style>
	</head>
	<body>
		<div id="controls-show">
			<span class="btn btn-info"><i class="fa fa-cogs fa-2x" aria-hidden="true"></i><br/>
			Show Settings</span>
		</div>
		<div id="controls">
			<div id="controls-hide">
				<span class="btn btn-info"><i class="fa fa-angle-double-right fa-2x" aria-hidden="true"></i><br/>
				Hide Settings</span><br/>
			</div><hr>
			 <div class="panel-group" id="controls-collapse">
				<div class="panel panel-primary">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#controls-collapse" href="#collapse1">
							Marker Settings</a>
						</h4>
					</div>
					<div id="collapse1" class="panel-collapse collapse in">
						<div class="panel-body">
						<div class="enhanced">
							<span class="markersToggle ">
								<p>
									<b>Nests</b>
									<div class="onoffswitch">
										<input type="checkbox" name="nests" class="onoffswitch-checkbox" id="nestsOnOff" checked>
										<label class="onoffswitch-label" for="nestsOnOff">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</p>
								<p>
								<b>Gyms</b>
									<div class="onoffswitch">
										<input type="checkbox" name="gyms" class="onoffswitch-checkbox" id="gymOnOff" >
										<label class="onoffswitch-label" for="gymOnOff">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</p>
								<p>
								<b>Pokestops</b>
									<div class="onoffswitch">
										<input type="checkbox" name="stops" class="onoffswitch-checkbox" id="stopOnOff" >
										<label class="onoffswitch-label" for="stopOnOff">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</p>
							</span>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-danger">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#controls-collapse" href="#collapse2">
							Heatmap Settings</a>
						</h4>
					</div>
					<div id="collapse2" class="panel-collapse collapse">
						<div class="panel-body">
						<div class="enhanced">
							<p>
								<b>All Spawns Heatmap</b>
								<div class="onoffswitch">
									<input type="checkbox" class="onoffswitch-checkbox" id="toggleHeatmap" >
									<label class="onoffswitch-label" for="toggleHeatmap">
											<span class="onoffswitch-inner"></span>
											<span class="onoffswitch-switch"></span>
									</label>
								</div>
							</p>
							</div>
							<div id="pokeHeatSelect" class="enhanced">
								<p>
								<b>Specific Pokemon Heatmap</b>
								<select class="form-control bottom-space" id="heat_mon" name="heat_mon">
									<?php include 'assets/includes/poke-options-by-id.php'; ?>
								</select>
								<span class="text-center  id="pokeHeatmapMessage"></span>
									<div class="onoffswitch">
										<input type="checkbox" class="onoffswitch-checkbox" id="togglePokeHeatmap" >
										<label class="onoffswitch-label" for="togglePokeHeatmap">
												<span class="onoffswitch-inner"></span>
												<span class="onoffswitch-switch"></span>
										</label>
									</div>
								</p>			
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-success">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#controls-collapse" href="#collapse3">
							Style Settings</a>
						</h4>
					</div>
					<div id="collapse3" class="panel-collapse collapse">
						<div class="panel-body">
						<div class="enhanced">
							<div class="btn-group btn-group-justified stylesToggle" style="padding:8px;" data-toggle="buttons" role="style-group">
								<label class="btn btn-primary active" name="pogo" role="button">
									<input type="checkbox"  autocomplete="off" checked> PoGo
								</label>
								<label class="btn btn-primary" name="vivid" role="button">
									<input type="checkbox" autocomplete="off"> Vivid
								</label>
								<label class="btn btn-primary" name="midnight" role="button">
									<input type="checkbox" autocomplete="off"> Night
								</label>
							</div>
							<div class="btn-group btn-group-justified stylesToggle" style="padding:8px;" data-toggle="buttons" role="style-group">
								<label class="btn btn-primary " name="satellite" role="button">
									<input type="checkbox"  autocomplete="off"> Satellite
								</label>
								<label class="btn btn-primary" name="roadmap" role="button">
									<input type="checkbox" autocomplete="off"> Roadmap
								</label>
								<label class="btn btn-primary" name="hybrid" role="button">
									<input type="checkbox" autocomplete="off"> Hybrid
								</label>
							</div>
							</div>
						</div>
					</div>
				</div>
				<div class="panel panel-warning">
					<div class="panel-heading">
						<h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#controls-collapse" href="#collapse4">
							Map / Location Suggestions</a>
						</h4>
					</div>
					<div id="collapse4" class="panel-collapse collapse">
						<div class="panel-body">
							<div class="enhanced"><h2 class="text-center">Coming Soon!</h2></div>
						</div>
					</div>
				</div>
			</div> 
			<strong>Show Help</strong><br/>
			<span id='getHelp' class='btn btn-lg btn-primary'><i class="fa fa-question-circle fa-2x" aria-hidden="true"></i></span>
		</div>
		<div id="progressWindow">
			<span class='text-center'>Getting Map Data</span> 
			<div class="progress">
				<div class="progress-bar progress-bar-striped active progress-bar-success " id="downloadProgressBar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;min-width:20px;">
					<span id='percentComplete'>0</span>%
				</div>
			</div>
		</div>
		
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-63915780-5', 'auto');
			ga('send', 'pageview');

		</script>
    <div id="map"></div>
		<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCea9Nyrzktqaw6nKtBzccDaMlbUNdJkck&callback=initMap&libraries=visualization">
    </script>
		<?php if ($_SERVER['SERVER_NAME'] != "localhost" || isset($_GET['min'])){ ?>
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/gmaps.KsbTKwvcVd.min.js"></script>
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/map.KsbTKwvcVd.min.js"></script>
			
		<?php } else { ?>
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/gmaps-minme.js"></script>
			<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/map-minme.js"></script>
			
		<?php } ?>
		
		
		<script src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/jquery.animateNumber.min.js"></script>
		
		<div class="modal fade" id="welcome_modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">Welcome to the New PokeMap! (Alpha)</h4>
					</div>
					<div class="modal-body">
						<div class="pokeball-wrapper">
							<img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokeball.png" />
						</div>
						<div class='enhanced text-center'>
							<h1>Nests Reset!</h1>
						</div>
						<div class='enhanced'>
							<p>Just a quick little update to let everyone know that the nests have been reset!</p>
							<p>This update is also here to let you know that this site isn't dead... entirely.. just probably won't see a TON of updates. Life is busy, and leaves little time for updates or Pokemon Go :( . I will still try and update nest locations when I can! Nest suggestions are EXTREMELY helpful in regards to this, as I rarely have the time to personally confirm locations.</p>
							<p>Happy hunting all!</p>
							<p class='text-right'><b>- BleedTheWay</b></p>
						</div>
					</div>
					<div class="modal-footer">
						<span class='pull-left'><input type="checkbox" id="hideWelcome" name="hideWelcome" /> Don't show this message again</span>
						<button type="button" class="btn btn-default" onclick="checkHideWelcome()" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="modal fade" id="suggest_modal" tabindex="-1" role="dialog">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">New PokeMap (Alpha)</h4>
					</div>
					<div class="modal-body" id="suggest-modal-body">
						<div class="pokeball-wrapper">
							<img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokeball.png" />
						</div>
						<div class='enhanced text-center'>
							<h2>Suggest Nest Pokemon</h2>
						</div>
						<div class='enhanced'>
							<p>Know which Pokemon spawns often at this nest? Select it from the list below and click submit!</p>
							<hr>
							<p id="suggestResponse"></p>
							<div id="suggestForm">
								
								<select class="form-control bottom-space" id="suggestion_mon" name="mon">
									<?php include 'assets/includes/poke-options.php'; ?>
								</select>
								<input type="hidden" id="suggestion_nestId" name="nestId" value="" />
								<input type="hidden" id="userIp" name="userIp" value="<?php echo $_SERVER['REMOTE_ADDR']; ?>" />
								<span class="btn btn-success form-control submitSuggestion" id="submitSuggestion">Submit</span>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
		<div class="modal fade" id="help_modal" tabindex="-1" role="dialog">
			<div class="modal-dialog modal-lg" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title">New PokeMap (Alpha)</h4>
					</div>
					<div class="modal-body map-help-body">
						<div class="pokeball-wrapper">
							<img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokeball.png" />
						</div>
						<div class='enhanced text-center'>
							<h2>Map Legend</h2>
						</div>
						
						<div class="row">
							<div class="col-md-6 col-s-12">
								<div class="enhanced">
									<h2>Unknown Nests</h2>
									<p>
										<img class="help-icon" src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_unknown.png" alt='Unknown Nest' />
										The Unknown Nest locations are spots that we know spawn a specific Pokemon often, we just currently are not sure of what Pokemon that is! This will happen when nests reset, until we have confirmation from the community as to what the new Pokemon in this location is. In the future there will be a built in method for making suggestion for new locations, or for what is spawning in nests. In the mean time, just shoot me a mention ("@bleedtheway") on our <a href="https://discord.gg/KKq68x8">discord server</a> to make a suggestion.
									</p>
								</div>
							</div>
							<div class="col-md-6 col-s-12">
								<div class="enhanced">
									<h2>Known Nests</h2>
									<p>
										<div id="nest_images">
											<img class='active' src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_nest1.png" alt='Unknown Nest' />
											<img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_nest2.png" alt='Unknown Nest' />
											<img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_nest3.png" alt='Unknown Nest' />
											<img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_nest4.png" alt='Unknown Nest' />
										</div>
										Locations with the image of a Pokemon are Known Nest Locations. These locations are areas in which the pictured Pokemon will spwan often. Bring your pokeballs, because you should catch quite a few of this mon in this area!
									</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6 col-s-12">
								<div style="min-height:200px;" class="enhanced">
									<h2>PokeStop</h2>
									<p>
										<img class="help-icon" src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_stop.png" alt='Unknown Nest' />
										Nothing too crazy here! These are locations of PokeStops in Pokemon Go.
									</p>
								</div>
							</div>
							<div class="col-md-6 col-s-12">
								<div style="min-height:200px;" class="enhanced">
									<h2>Gym</h2>
									<p>
										<img class="help-icon" src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/map/icons/help_gym.png" alt='Unknown Nest' />
										Pretty straight forward. These are locations of Gyms in Pokemon Go.
									</p>
								</div>
							</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-default" onclick="clearTimeout(swap)" data-dismiss="modal">Close</button>
					</div>
				</div><!-- /.modal-content -->
			</div><!-- /.modal-dialog -->
		</div><!-- /.modal -->
  </body>
</html>