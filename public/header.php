<?php if(!isset($pageTitle)){$pageTitle = "PoGomaha";}; ?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta name="description" content="<?php echo (isset($metaDescription))?$metaDescription:"A site for nothing but Pokemon Go in Omaha, Nebraska! Tools, a Community built map, and more!"; ?>">
		<meta name="author" content="BleedTheWay & Fuse">
		<title><?php echo isset($pageTitle)?$pageTitle:"PoGomaha";?></title>
		<?php
		if(isset($jsBsInHeader) && $jsBsInHeader){
			?>
			<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/jquery.min.js'></script>
			<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/bootstrap.min.js'></script>
			<?php
		}
		?>
		<link rel="stylesheet" media="screen" href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,700">
		<link rel="stylesheet" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/font-awesome-4.6.3/css/font-awesome.min.css">
		<link href='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/bootstrap.min.css' rel=stylesheet>
		
		<?php if ($_SERVER['SERVER_NAME'] != "localhost" || isset($_GET['min'])){ ?>
			<link href='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/custom.KsbTKwvcVd.min.css' rel=stylesheet>		
		<?php } else { ?>
			<link href='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/css/custom-minme.css' rel=stylesheet>		
		<?php } ?>
		<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
		<link rel="icon" type="image/png" href="/favicon-32x32.png" sizes="32x32">
		<link rel="icon" type="image/png" href="/favicon-16x16.png" sizes="16x16">
		<link rel="manifest" href="/manifest.json">
		<meta name="theme-color" content="#ffffff">
		<?php echo isset($addHead)?$addHead:"";?>
		<meta property="og:description" content="A site for nothing but Pokemon Go in Omaha, Nebraska! Tools, a Community built map, and more!" />
		<meta property="og:url" content="http://pogomaha.com" />
		<meta property="og:title" content="PoGomaha" />
		<meta property="og:image:width" content="1200" />
		<meta property="og:image:height" content="563" />
		<meta property="og:image" content="http://pogomaha.com/assets/img/web/pokemongomaha.png" />
		<style>
		<?php echo isset($addCSS)?$addCSS:"";?>
		</style>
	</head>
	<body id='body'>
		<script>
			(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
			(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
			})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

			ga('create', 'UA-63915780-5', 'auto');
			ga('send', 'pageview');

		</script>
		<?php 
		(!isset($noNav))?$noNav=true:false;
		if (!$noNav){ ?>
		<nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/"><img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokemongomaha60x28.png"></a></div>
        <div id="navbar" class="navbar-collapse collapse">

          <ul class="nav navbar-nav">
            <li class="<?php echo ($pageTitle == "PoGomaha | Home")?"active":""; ?>"><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/"><img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/PokeHome.png" width="28px" height="28"></img> Home</a></li>
            <!--- <li><a href="#">About</a></li> --->
            <li class="dropdown">
							<a href="#" class="dropdown-toggle <?php echo ($pageTitle == "PoGomaha | Map")?"active ":""; ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokemap.jpg" width="28px" height="28"></img> PokeMap <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/maps.php":"/maps"; ?>">Original PokeMap</a></li>
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/map2.php":"/map2"; ?>">New PokeMap (Alpha Version)</a></li>
							</ul>
						</li>
            <li class="dropdown">
							<a href="#" class="dropdown-toggle <?php echo ($pageTitle == "PoGomaha | Tools")?"active ":""; ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/tools_icon.jpg" width="28px" height="28"></img> Tools <span class="caret"></span></a>
							<ul class="dropdown-menu">
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/evocalc.php":"/tools/evocalc"; ?>">Egg / Evolution Calculator</a></li>
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/":"/tools/#/"; ?>">IV Calculator</a></li>
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/power/":"/tools/#/power/"; ?>">Power Up Calculator</a></li>
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/moves/":"/tools/#/moves/"; ?>">Best Moves List</a></li>
								<li><a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/matchup/":"/tools/#/matchup/"; ?>">Best Matchup List</a></li>
							</ul>
						</li>
            <li><a href="https://discord.gg/BPXgaD4" target="_blank"><img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/discord.png" width="28px" height="28" title="Join The Omaha Discord Server Today!"></img> Join Discord</a></li>
			<li><a href="https://www.reddit.com/r/gomaha" target="_blank"><img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/network_social_reddit.png" width="28px" height="28" title="Check out the Pokemon Go Subreddit Page!"></img> GOmaha Subreddit</a></li>
            <!--- <li><a href="#">Contact</a></li> --->
          </ul>

        </div><!--/.navbar-collapse -->
      </div>
    </nav>
		<?php }; ?>