<?php 
$pageTitle="PoGomaha | Map";
$noNav=false;
include 'header.php';
?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
     <nav class="jumbo-nav">
      <div class="navigation-wrapper">
        <div class="navigation">
          <div class="header-logo">
            <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/">
              <img src="assets/img/web/pokemongomaha.png" height="176" />
            </a>
          </div>
        </div>
      </div>
    </nav>

    <div class="container">
			<div class="row">
				<div class="col-md-12">
					<div class="enhanced">
            <h1><a href="https://www.google.com/maps/d/u/0/embed?mid=1K8cgQlPCcGfk-l2akwwXYmvT6Dw">PokeMon Map</a></h1>
            <h5>Friday, August 19, 2016</h5>
						<iframe src="https://www.google.com/maps/d/u/0/embed?mid=1K8cgQlPCcGfk-l2akwwXYmvT6Dw" width="100%" height="800"></iframe>
          </div>
        </div>
      </div>
    </div>
<?php include 'footer.php'; ?>