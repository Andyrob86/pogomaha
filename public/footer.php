<div id='footer' class="footer_b top-space footer-cont" style="margin-bottom:0px;">
	<div class="container-fluid">
		<div class="row">
			
			<div class="col-md-4 widget">
				<div class="widget-body">
					<p>
						<a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/">Home </a> | 
						<!--- <a href="/about"> About </a> | --->
						<a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/maps.php":"/maps"; ?>"> Map </a> |
						<a href="https://discord.gg/BPXgaD4" target="_blank"> Join Discord </a>
						<!--- <a href="mailto:bleedtheway@pogomaha.com"> Contact</a> --->
					</p>
				</div>
			</div>
			
			<div class="col-md-4 widget text-center">
				<div class="widget-body">
					<p>
						<?php echo isset($addFooterMid)?$addFooterMid:"";?>
					</p>
				</div>
			</div>

			<div class="col-md-4 widget">
				<div class="widget-body">
					<p class="text-right">
						Copyright &copy; 2016, PoGOmaha.com. Not affiliated with Niantic Labs, Nintendo, or The Pokemon Company.
					</p>
				</div>
			</div>

		</div>
	</div>
</div>

<?php
if (!isset($jsBsInHeader)){
?>
<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/jquery.min.js'></script>
<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/bootstrap.min.js'></script>

<script type='text/javascript'>
$( document ).ready(function() {
	var dabestis = new x228(function() { dabest();});

	$(function () {
		$('[data-toggle="tooltip"]').tooltip({
			trigger: "click"
		})
	})

});
</script>
<?php	
}
?>
<?php if ($_SERVER['SERVER_NAME'] != "localhost" || isset($_GET['min'])){ ?>
	<script type="text/javascript" src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/tooltip.KsbTKwvcVd.min.js"></script>
	<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/js.cookie.KsbTKwvcVd.min.js'></script>
	<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/283243.KsbTKwvcVd.min.js'></script>
<?php } else { ?>
	<script type="text/javascript" src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/tooltip-minme.js"></script>
	<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/js.cookie-minme.js'></script>
		<script src='<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/js/283243-minme.js'></script>

<?php } ?>
<script type="text/javascript"><?php echo (isset($addScriptFooter))?$addScriptFooter:""; ?></script>

</body>
</html>