<?PHP // Start the php code. Single comment lines in php are //. Multiline comments are done with /* like below.

$addHead="<meta property='og:url' content='http://www.url.com' />"; // add code between the <head> and </head> tag.

$pageTitle="PoGomaha | Home"; // I'm sure you get it lol.. but this will change the <title> tag. Default will be "PoGomaha"

$addCSS=".example_css{margin-bottom:0px}"; // create a variable name "addCSS" as a text string of CSS to add custom css to the page. SHOULD overwrite existing css per page... css...

$noNav=false; // if set to true this will hide the navigation bar <-- THIS HAS ISSUES RIGHT NOW.. WILL FIX!! Just keep it as is.

$page="Home"; // Setting this to "Home" or "PokeMap" will set the active li within the menu

// now on to actually including the <head> code and navigation

include 'header.php'; 

// now to end the PHP code and go back to HTML

?>

	 <nav class="jumbo-nav">
		<div class="navigation-wrapper">
			<div class="navigation">
				<div class="header-logo">
					<a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/">
						<img src="assets/img/web/pokemongomaha.png" height="176" />
					</a>
				</div>
			</div>

			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="pokeball-wrapper">
							<img src="assets/img/web/pokeball.png" />
						</div>
						<div class="enhanced text-center">
							<h1 style="font-size:40px">Welcome Trainers!</h1>
							
							<div class="row">
								<div class="col-xs-8 col-xs-offset-2">
								<hr>
									<p style="font-size:20px"><b>PoGomaha.com</b> is a hub for everything Pokemon Go in Omaha, Nebraska. From things like IV and Evolution calculators, to local maps and more!</p>
									<p style="font-size:20px">Join in the discussion with other local trainers on <a href="https://discord.gg/BPXgaD4" target="_blank">Discord</a>! Learn about Pokemon spawn locations, organize a gym takover with your team, or just chat it up!</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</nav>
    
    <div class="container">
      <hr>
			
      <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
            <h1>New Feature - PokeSpawn Heatmap!</h1> 
            <h5>Tuesday, August 30, 2016</h5>
            <p><b>The PokeSpawn Heatmap</b> is a heatmap of known Pokemon spawn locations found around Omaha. Read more about it in the welcome message on the new map!</p>
            <p><b>Happy hunting trainers!</p>
						<p class="text-right"> - BleedTheWay  </p>
          </div>
        </div>
      </div>
			
      <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
            <h1>New Feature - Pokemon Go Tools!</h1> 
            <h5>Monday, August 22, 2016</h5>
            <p>Hey everyone! Just wanted to give you all a quick heads up that a new feature has been added to PoGomaha.com! The Pokemon Go "Tools" Section!</p>
						<p>
							You will find some great new features in there for you to <i class="fa fa-music" aria-hidden="true"></i><i> be the very best, like no one ever was! </i><i class="fa fa-music" aria-hidden="true"></i> The new tools include:
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/evocalc.php":"/tools/evocalc/"; ?>"><i class="fa fa-calculator" aria-hidden="true"></i> Lucky Egg Evolution Calculator</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>A calculator to help you get the most out of your lucky eggs! Enter the number of Pokemon / Candy you have available to determine how much XP you will gain, and how long it will take to complete your evolutions. Get as close to the 30 minute mark as you can to max out your XP gains!</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/":"/tools/#/"; ?>"><i class="fa fa-calculator" aria-hidden="true"></i> Pokemon Individual Value (IV) Calculator</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>With the PoGo IV Calculator you can get a better idea of your pokemon's Individual values. Know if you should hang on to that pokemon, or send it to Willow's grinder!</p>
									<p>If your unsure of what Individual Values are, here is a <a href="https://www.reddit.com/r/TheSilphRoad/comments/4tupg5/eli5_what_is_iv_and_what_is_perfect/">great "Explain like I'm 5" article</a> by <a href="https://www.reddit.com/user/Conan-The-Librarian">/u/Conan-The-Librarian</a> on reddit.</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/power/":"/tools/#/power/"; ?>"><i class="fa fa-calculator" aria-hidden="true"></i> Power Up Calculator</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>The Power Up Calculator will help you determine how much stardust you will need to level a given pokemon to max. Just enter your pokemon's information, and find out how much dust you will need to max them out!</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/moves/":"/tools/#/moves/"; ?>"><i class="fa fa-th-list" aria-hidden="true"></i> Best Moveset List</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>The Best Moveset List shows dps per moveset. DPS is assumed that you attack with the quick attack nonstop, and with power attack as soon as it is available.</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/matchup/":"/tools/#/matchup/"; ?>"><i class="fa fa-th-list" aria-hidden="true"></i> Best Matchup List</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>The Best Matchup List is... well... You guessed it! A list of best possible battle matchups per pokemon! Select the pokemon you are going to fight, and get a list of the best matchups vs that pokemon.</p>
								</div>
							</div>
							
							
						</p>
						<p>I hope everyone finds the new tools useful, and happy hunting all!</p>
						<p class="text-right"> - BleedTheWay  </p>
          </div>
        </div>
      </div>
			
      <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
            <h1>Community Update!</h1> 
            <h5>Friday, August 19, 2016</h5>
            <p>Welcome to our awesome Live Action Packed Website! Please bare with us as most of the content is still being added daily. We will do our very best to push Updates on our site for you as much as we possibly can. As in Map Updates / Bot Updates &gt; New Command Features like [ !iv ], [ !memehelp ] and more!. I will be creating separate pages soon for all the awesome benefits you are able to use very soon so you have a better understanding. Thanks for taking the time to read this very brief and short post today!. Have a great day and hope to chat with you on the Omaha Discord Server!. </p>
          </div>
        </div>
      </div>
       <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
<h1>Discord Server</h1> 
            <h5>Friday, August 19, 2016</h5>
            <iframe src="https://discordapp.com/widget?id=204159140736663552&theme=dark" width="100%" height="500" allowtransparency="true" frameborder="0"></iframe>
          </div>
        </div>
      </div>
		</div>
<?php include 'footer.php'; ?>