<?PHP 
$root = ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":"";
$addHead="<link rel='stylesheet' href='css/lotus.css'><link rel='stylesheet' href='{$root}/assets/css/custom.css'><link rel='stylesheet' href='react-select.css'><!--<script src='https://cdn.rawgit.com/naptha/tesseract.js/master/lib/Tesseract.2015.07.26.js'></script>-->";
$addCSS="
			html, body {
        margin: 0;
        height: 100%;
        min-height: 100%;
      }

      #app {
        height: 100%;
      }
      input {
        box-sizing: border-box;
        width: 100%;
      }

      .btn {
        color: #fff;
      }

      .nav a.active {
        background-color: #FFCB05;
      }

      .label {
        border-radius: 4px;
        display: inline-block;
        font-size: 0.75em;
        text-align: center;
        width: 2.5em;
      }

      .table th, .table td {
        padding: 0.25em;
      }

      .table.border {
        border: 1px solid #d0d0d0;
      }

      .table.border tbody>:nth-child(2n-1) {
        border-bottom: 1px solid #d0d0d0;
        border-top: 1px solid #d0d0d0;
      }

      .table.border td {
        border-left: 1px solid #d0d0d0;
      }

      .table.clean tbody>:nth-child(2n-1) {
        background-color: inherit;
      }
    </style>";

$pageTitle="PoGomaha | Tools";
$noNav=false;include '../header.php'; 
?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
     <nav class="jumbo-nav">
      <div class="navigation-wrapper">
        <div class="navigation">
          <div class="header-logo">
            <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/">
              <img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokemongomaha.png" height="176" />
            </a>
          </div>
        </div>
      
        <!-- Example row of columns -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-s-4 col-s-offset-4">
							<div class="pokeball-wrapper">
                <img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokeball.png" />
              </div>
              <div class="enhanced text-center">
                <h1>Pokemon Go Tools</h1>
                <strong>Pokemon IV calculator, Moves List, Power Up Calculator, and Best Matchup Guide</strong>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </nav>

		<div class="container-fluid top-space">
			<div class="row">
				<div class="col-xs-12">
					<div id="app" class="enhanced"></div>
					<canvas id="capturedPhoto" style="position: absolute; left: -1000em"></canvas>
				</div>
			</div>
		</div>
<script src="build.js"></script>
<?php

$addFooterMid = "PoGomaha Poke Tools are brought to you by <a href='https://github.com/goatslacker/pokemon-go-iv-calculator'>GoatSlacker</a>.";
include '../footer.php' 

?>