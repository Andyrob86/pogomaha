<?php
$noNav = false;
$jsBsInHeader = true;
$pageTitle = "PoGomaha | Tools";
$addHead = '<script type="text/javascript" src="https://code.jquery.com/ui/1.12.0/jquery-ui.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.3.2/js/tether.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether-drop/1.4.2/js/drop.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/tether-tooltip/1.2.0/js/tooltip.min.js"></script>
  <script type="text/javascript" src="js/calc.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/tether-tooltip/1.2.0/css/tooltip-theme-arrows.min.css">
  <link rel="stylesheet" type="text/css" href="https://code.jquery.com/ui/1.12.0/themes/base/jquery-ui.css">
	<link rel="stylesheet" type="text/css" href="css/calcstyle.css">';

include '../header.php';
?>
	    <!-- Main jumbotron for a primary marketing message or call to action -->
     <nav class="jumbo-nav">
      <div class="navigation-wrapper">
        <div class="navigation">
          <div class="header-logo">
            <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/">
              <img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokemongomaha.png" height="176" />
            </a>
          </div>
        </div>
      
        <!-- Example row of columns -->
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-s-4 col-s-offset-4">
							<div class="pokeball-wrapper">
                <img src="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/assets/img/web/pokeball.png" />
              </div>
              <div class="enhanced text-center">
                <h1>Lucky Egg Evolution Calculator</h1>
                <strong>Make sure you are getting the most out of your lucky eggs!</strong>
              </div>
            </div>
          </div>
        </div>
        
      </div>
    </nav>
	
  <div class="section top-space">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-10 col-md-offset-1">
					<div class="enhanced">
						<h4>
							The Lucky Egg Evolution Calculator will help you determine how many evolutions you can make, how much XP you will get, and how long it will take to make those evolutions so you can utilize every minute of your Lucky Egg!
						</h4>
						<p>
							<b>NOTE: If a pokemon cannot be evolved, (i.e., Beedrill), it will not show up in the auto-fill.</b>
						</p>
						<!--           <p>
							TODO: Eevee evolutions, clean up code.
						</p> -->
					</div>
        </div>
			</div>
			<div class="row">
        <div class="col-xs-12 col-md-6">
					<div class="hide-calc enhanced">
						<h1>Total XP: <span  id="totxp" class="totalxp"></span></h1>
						<p class="line1">Without lucky egg: <span id="xpw"></span>
						</p>
						<p class="line2">Approx Time Required in Minutes: <span id="time"></span>
						</p>
						<p class="line3">Total Transfers: <span id="trans"></span>
						</p>
						<p class="line4">Total Evolutions: <span id="evo"></span>
						</p>
					</div>
        </div>
        <div class="col-xs-12 col-md-6">
					<div class="hide-calc enhanced">
						<div id="instructions">
							<h2 class='text-center'>Instructions</h2>
							<hr>
							<ol id="instlist">
							</ol>
						</div>
					</div>


        </div>
      </div>
    </div>

    <div class="modal fade" id="modal-1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
            <h4 class="modal-title"> [TITLE] </h4>
          </div>
          <div class="modal-body">
						
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-success" data-dismiss="modal">Close</button>
          </div>
        </div>
      </div>
    </div>

  </div>
	
	<div class="container">
		<h1>
			<abbr rel="tooltip" title="When you're done filling out one or more rows, you can generate a list of steps to take to achieve <em><ins>Maximum XP</ins></em>!">
				<button id="runProgram" class="btn btn-success btn-lg"  >Generate Instructions <span class="fa fa-arrow-right"></span></button>
			</abbr>
		</h1>
	</div>

  <div class="section">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
					<div class="enhanced">
						<div class="table-responsive">
							<table class="table table-hover">
								<thead>
									<tr>
										<th>Pokemon</th>
										<th># in Inventory</th>
										<th>Candies Owned</th>
										<th>Evo in Pokedex?</th>
										<th>Recycle Evos?</th>
									</tr>
								</thead>
								<tbody id="rowBody">
									<tr id="input-0" class="tableRow">
										<td>
											<abbr rel="tooltip" title="The name of a pokemon in your inventory."><input class="form-control" id="pok-0" type="text" value="Pidgey" placeholder="Pokemon" ></abbr>
										</td>
										<td>
											<abbr rel="tooltip" title="How many of this type of pokemon are in your inventory?"><input class="form-control" type="number" value="13" placeholder="Pokemon in Inventory"></abbr>
										</td>
										<td>
											<abbr rel="tooltip" title="How many candies for this pokemon do you have?"><input class="form-control" type="number" value="23" placeholder="Candies Owned"></abbr>
										</td>
										<td>
											<abbr rel="tooltip" title="Do you have the evolved form of this pokemon?">
												<div class="btn-group btn-block" data-toggle="buttons">
													<label class="btn btn-danger btn-block active check"  >
														<label style="vertical-align: bottom;" autocomplete="off">
															<input id="chk1-1" type="checkbox" checked>
															<i style="vertical-align: bottom; margin-bottom:-5px; font-size:20px" class="fa fa-check"></i><i class="fa fa-times" style="vertical-align: bottom; margin-bottom:-5px; font-size:20px; display:none"></i>
														</label>
													</label>
												</div>
											</abbr>
										</td>
										<td>
											<abbr rel="tooltip" title="Do you want to transfer the evolved form for more candy?">
												<div class="btn-group btn-block" data-toggle="buttons">
													<label class="btn btn-danger btn-block active check" >
														<label style="vertical-align: bottom;" autocomplete="off ">
															<input id="chk2-1" type="checkbox" checked>
															<i style="vertical-align: bottom; margin-bottom:-5px; font-size:20px" class="fa fa-check"></i><i class="fa fa-times" style="vertical-align: bottom; margin-bottom:-5px; font-size:20px; display:none"></i>
														</label>
													</label>
												</div>
											</abbr>
										</td>
									</tr>

								</tbody>
							</table>
						</div>
						<button id="addRow" class="btn btn-info">Add Another <span class="fa fa-plus"></span></button>
					</div>
        </div>
      </div>
    </div>
  </div>
	
	<?php
	$addFooterMid = '<p class="text-muted ">Lucky Egg Calculator created by <a href="http://github.com/tehalexf">tehalexf</a> and <a href="http://github.com/thinkaliker">thinkaliker</a>.</p>';
	include '../footer.php';
	?>