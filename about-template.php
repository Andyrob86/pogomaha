<?PHP // Start the php code. Single comment lines in php are //. Multiline comments are done with /* like below.

$addHead="<meta property='og:url' content='http://www.url.com' />"; // add code between the <head> and </head> tag.

$pageTitle="PoGomaha | Home"; // I'm sure you get it lol.. but this will change the <title> tag. Default will be "PoGomaha"

$addCSS=".example_css{margin-bottom:0px}"; // create a variable name "addCSS" as a text string of CSS to add custom css to the page. SHOULD overwrite existing css per page... css...

$noNav=false; // if set to true this will hide the navigation bar <-- THIS HAS ISSUES RIGHT NOW.. WILL FIX!! Just keep it as is.

$page="Home"; // Setting this to "Home" or "PokeMap" will set the active li within the menu

// now on to actually including the <head> code and navigation

include 'header.php'; 

// now to end the PHP code and go back to HTML

?>

    <!-- Main jumbotron for a primary marketing message or call to action -->
     <nav class="jumbo-nav">
      <div class="navigation-wrapper">
        <div class="navigation">
          <div class="header-logo">
            <a href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public":""; ?>/">
              <img src="assets/img/web/pokemongomaha.png" height="176" />
            </a>
          </div>
        </div>
      
        <!-- Example row of columns -->
        <div class="container">
          <div class="row">
            <div class="col-md-12">
              <div class="pokeball-wrapper">
                <img src="assets/img/web/pokeball.png" />
              </div>
              <div class="enhanced">
                <h2>About Pogomaha.com</h2>
                <img class="pokemon small" src="assets/img/web/pikachu1.png" />
                <p>Welcome to <b>Pogomaha.com</b>, How we have become what we are today!  It all started with the Omaha Pogo Discord Server <img src="assets/img/web/discord.png" width="28px" height="28"></img></li>. We launched the Discord server only a few days after Pokemon Go came alive!</p>
                <p>We started our first project for the community with the <b>PokeEverything Google Maps</b>.  This project still lives today but will soon come to and end!  But have no fear its prodigy is now in it's Alpha Stage.  You can see this new map from heading to the navigation bar and clicking PokeMap and selecting <b>New PokeMap (Alpha Verison)</b> </p>
                <p>Shortly amonth later we decided to start creating tools for Pogo Players benefits like Live Maps, IV Tools, Egg / Evolution Calculator, Power Up Calculator, Best Moves List, Best Matchup List.  Then we said hey? We need a place to put all of this awesome information for people to quickly access and use! Thus is how <b>Pogomaha.com</b> was created!</p>
                <!--
							<div class="pokeball-wrapper">
								<img src="assets/img/web/pokeball.png" />
							</div>
                            
							<div class="enhanced">
								<h2>@BleedTheWay</h2>
								<img class="pokemon small" src="assets/img/web/BleedTheWaY.jpg" title="The Konami Code FTW! ;)" tip-type="click" data-toggle="tooltip" data-placement="top"/>
								<p>Hello! BleedTheWay Here!</p>
								<p>I'm the Founder Of PoGomaha.com, creator of BleedBot, and an admin on our Pokemon Go Omaha Discord Server! I Love developing awesome things for the community to enhance their Pokemon Go experience and more!</p>
						 </div>
						</div>
            <div class="col-md-4">
              <div class="pokeball-wrapper">
                <img src="assets/img/web/pokeball.png" />
              </div>
              <div class="enhanced">
                <h2>@Rushy</h2>
                <img class="pokemon small" src="assets/img/web/Rushy.png" />
                <p>Hello! Rushy Here!</p>
                <p>I'm a very busy person IRL. &nbsp;I do my best to pop on and hang out with this awesome community as much as I possibly can! I am an Admin on the Pokemon Go Omaha Discord Server and have enjoyed every second of it thus far!. </p>
              </div>
            </div>
          </div>
        </div>
        -->
      </div>
    </nav>
    
    <div class="container">
      <hr>
			
      <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
            <h1>New Feature - Pokemon Go Tools!</h1> 
            <h5>Monday, August 22, 2016</h5>
            <p>Hey everyone! Just wanted to give you all a quick heads up that a new feature has been added to PoGomaha.com! The Pokemon Go "Tools" Section!</p>
						<p>
							You will find some great new features in there for you to <i class="fa fa-music" aria-hidden="true"></i><i> be the very best, like no one ever was! </i><i class="fa fa-music" aria-hidden="true"></i> The new tools include:
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/evocalc.php":"/tools/evocalc/"; ?>"><i class="fa fa-calculator" aria-hidden="true"></i> Lucky Egg Evolution Calculator</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>A calculator to help you get the most out of your lucky eggs! Enter the number of Pokemon / Candy you have available to determine how much XP you will gain, and how long it will take to complete your evolutions. Get as close to the 30 minute mark as you can to max out your XP gains!</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/":"/tools/#/"; ?>"><i class="fa fa-calculator" aria-hidden="true"></i> Pokemon Individual Value (IV) Calculator</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>With the PoGo IV Calculator you can get a better idea of your pokemon's Individual values. Know if you should hang on to that pokemon, or send it to Willow's grinder!</p>
									<p>If your unsure of what Individual Values are, here is a <a href="https://www.reddit.com/r/TheSilphRoad/comments/4tupg5/eli5_what_is_iv_and_what_is_perfect/">great "Explain like I'm 5" article</a> by <a href="https://www.reddit.com/user/Conan-The-Librarian">/u/Conan-The-Librarian</a> on reddit.</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/power/":"/tools/#/power/"; ?>"><i class="fa fa-calculator" aria-hidden="true"></i> Power Up Calculator</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>The Power Up Calculator will help you determine how much stardust you will need to level a given pokemon to max. Just enter your pokemon's information, and find out how much dust you will need to max them out!</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/moves/":"/tools/#/moves/"; ?>"><i class="fa fa-th-list" aria-hidden="true"></i> Best Moveset List</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>The Best Moveset List shows dps per moveset. DPS is assumed that you attack with the quick attack nonstop, and with power attack as soon as it is available.</p>
								</div>
							</div>
							
							<ul><li><h3><a style="text-decoration:none;color:black" href="<?php echo ($_SERVER['SERVER_NAME'] == "localhost")?"/pogomaha/public/tools/#/matchup/":"/tools/#/matchup/"; ?>"><i class="fa fa-th-list" aria-hidden="true"></i> Best Matchup List</a></h3></li></ul>
							<div class="row">
								<div class="col-xs-10 col-xs-offset-1">
									<p>The Best Matchup List is... well... You guessed it! A list of best possible battle matchups per pokemon! Select the pokemon you are going to fight, and get a list of the best matchups vs that pokemon.</p>
								</div>
							</div>
							
							
						</p>
						<p>I hope everyone finds the new tools useful, and happy hunting all!</p>
						<p class="text-right"> - BleedTheWay  </p>
          </div>
        </div>
      </div>
			
      <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
            <h1>Community Update!</h1> 
            <h5>Friday, August 19, 2016</h5>
            <p>Welcome to our awesome Live Action Packed Website! Please bare with us as most of the content is still being added daily. We will do our very best to push Updates on our site for you as much as we possibly can. As in Map Updates / Bot Updates &gt; New Command Features like [ !iv ], [ !memehelp ] and more!. I will be creating separate pages soon for all the awesome benefits you are able to use very soon so you have a better understanding. Thanks for taking the time to read this very brief and short post today!. Have a great day and hope to chat with you on the Omaha Discord Server!. </p>
          </div>
        </div>
      </div>
       <div class="row">
        <div class="col-md-12">
          <div class="enhanced">
<h1>Discord Server</h1> 
            <h5>Friday, August 19, 2016</h5>
            <iframe src="https://discordapp.com/widget?id=204159140736663552&theme=dark" width="100%" height="500" allowtransparency="true" frameborder="0"></iframe>
          </div>
        </div>
      </div>
		</div>
		

<!--- WOAH... missing footer and another PHP code block... ohhh yea baby.... sorry.. geting tired lol --->

<?php
/* you probably guessed it... including all the footer information this time. This time with the "beefed up" footer combo from my design and yours. 
Kept the mini nav, removed the designed by, and discord widget. Should still add more contact info really, want a fuse@pogomaha.com email??
I can also create a contact widget or something else that would prevent access to a direct email address. Something like

https://overwatchmm.com/contact


well.. I think that is all my comments for now.. if you have any questions on this drawn out comment filled update just let me know...
*/

include 'footer.php' 

?>