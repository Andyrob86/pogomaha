<?php
ini_set('memory_limit','256M');
if ($_SERVER['SERVER_NAME'] == "localhost" || strpos($_SERVER['SERVER_NAME'], 'ngrok.io') !== false){
	include '/../../vendor/autoload.php';
}	else {
	include '/home/arobvps/pogomaha.com/vendor/autoload.php';
}

if (isset($_POST['suggestion']) && $_POST['suggestion'] == 1){
	if (isset($_POST['mon']) && isset($_POST['nestId'])){
		$db = new DB();
		$nid = $_POST['nestId'];
		$time = date('Y-m-d H:i:s', $_SERVER['REQUEST_TIME']);
		$ip = $_POST['userIp'];
		$mon = $_POST["mon"];
		$sugg = new nestSugg();
		$sugg->nest_id = $nid;
		$sugg->mon = $mon;
		$sugg->timesuggested = $time;
		$sugg->ip = $ip;
		if ($sugg->Create() > 0){
			$response['message'] = "Thanks for the suggestion!";
			$response['state'] = "success";
			echo json_encode($response);
		} else {
			$response['message'] = "Something went wrong trying to add your suggestion 1 :(";
			$response['state'] = "fail";
			echo json_encode($response);
		}

		
	} else {
		$response['message'] = "Something went wrong trying to add your suggestion 2 :(";
		$response['state'] = "fail";
		echo json_encode($response);
	}
	
}

// if (isset($_GET['data']) && $_GET['data'] == 1){
if (isset($_POST['data']) && $_POST['data'] == 1){
	$locations = array();
	$db = new DB();
	
	if ($results = $db->query("select latitude AS lat, longitude AS lng from pokestop")){
		foreach ($results as $key => $result){
			$iw['content']="PokeStop";
			$result['infoWindow']=$iw;
			$result['icon']="assets/img/map/icons/pokestop_small.png";
			$locations['stops'][] = $result;
		}
	}
	
	
	
if ($results = $db->query("select pokemon_id AS id, latitude AS lat, longitude AS lng from pokemon")){
	foreach ($results as $key => $result){
		
		$locations['heatmap'][] = $result;
		
		
		
		// if (!array_key_exists($result['id'],$locations['pokemons'])){
			// $locations['pokemons'][$result['id']] = [];
			// $result['icon']['url'] = $result['id'];
			// for ($x = 0; strlen($result['icon']['url']) < 3; $x++) {
				// $result['icon']['url'] = "0".$result['icon']['url'];
			// }
			// $result['icon']['url']= "assets/img/map/pokemon/png/".$result['icon']['url']."-1.png";
			// $locations['spawnpoints'][] = $result;
			
			// $locations['pokemons'][$result['id']][] = $result;
			
		// } else {
			
			// foreach($locations['pokemons'][$result['id']] as $locKey => $monData){
				// print_r($monData);
					// echo "<br/><hr><br/>";
					
					
				// if ($monData['lat'] == $result['lat'] && $monData['lng'] == $result['lng']){
					// if (array_key_exists('weight',$locations['pokemon'][$result['id']][$locKey])){
						// echo "adding weight";
						// $locations['pokemon'][$result['id']][$locKey]['weight'] += 1; 
					// } else {
						// $locations['pokemon'][$result['id']][$locKey]['weight'] = 2; 
					// }
					
				// }
				
				
			// }
			
		// }
		
		
/* 
			// $iw['content']="<div class='iwContent'><h3>{$result['id']} spawn point</h3></div>";
			//$result['infoWindow']=$iw;
			$result['sx']=80;
			$result['sy']=80;
			$result['ax']=40;
			$result['ay']=40;
			$result['ox']=0;
			$result['oy']=0;
			$result['icon']['url'] = $result['id'];
			for ($x = 0; strlen($result['icon']['url']) < 3; $x++) {
				$result['icon']['url'] = "0".$result['icon']['url'];
			}
			$result['icon']['url']= "assets/img/map/pokemon/png/".$result['icon']['url']."-1.png";
			$locations['spawnpoints'][] = $result;
 */
	}
	
	
	
		// print_r($locations['pokemon']);
}

/* 	
	$db->query("select pokemon_id, latitude AS lat, longitude AS lng from pokemon");
	if ($results = $db->resultset()){
	foreach ($results as $key => $result){
			$iw['content']="<div class='iwContent'><h3>{$result['pokemon_id']} spawn point</h3></div>";
			$result['infoWindow']=$iw;
			$result['sx']=80;
			$result['sy']=80;
			$result['ax']=40;
			$result['ay']=40;
			$result['ox']=0;
			$result['oy']=0;
			$result['icon']['url'] = $result['pokemon_id'];
			for ($x = 0; strlen($result['icon']['url']) < 3; $x++) {
				$result['icon']['url'] = "0".$result['icon']['url'];
			}
			$result['icon']['url']= "assets/img/map/pokemon/png/".$result['icon']['url']."-1.png";
			$locations['pokemons'][] = $result;
		}
		
	}
	 */
	 
	 
	if ($results = $db->query("SELECT pokemon_id as id, latitude as lat, longitude as lng FROM pokemon")){
		foreach($results as $key => $result){
			$id = $result['id'];
			unset($result['id']);
			
/* 		
			$time = date_format(new DateTime($result['time']),"i");
			$time = strtotime("$time -15 minutes");
			$iw['content']="<div class='iwContent'><p>Estimated spawn time: {$time}</p></div>";
			$result['infoWindow']=$iw;
			$result['sx']=20;
			$result['sy']=20;
			$result['ax']=10;
			$result['ay']=10;
			$result['ox']=0;
			$result['oy']=0;
			$result['icon']['url'] = $id;
			for ($x = 0; strlen($result['icon']['url']) < 3; $x++) {
				$result['icon']['url'] = "0".$result['icon']['url'];
			}
			$result['icon']['url']= "assets/img/map/pokemon/png/".$result['icon']['url']."-1.png";

 */
			$locations['pokemons'][$id][] = $result;
		}
		// print_r($locations['pokemons']);
	}
	
	
	if ($results = $db->query("select latitude AS lat, longitude AS lng from gym")){
		foreach ($results as $key => $result){
			$iw['content']="Gym";
			$result['infoWindow']=$iw;
			$result['icon']="assets/img/map/icons/GO_Gym.png";
			$locations['gyms'][] = $result;
		}
	}
	
	if ($results = $db->query("SELECT nest_id, lat, lon AS lng, name, id FROM pogomap.nests LEFT JOIN pogomap.pokemon_info on current_pokemon = id")){
		foreach ($results as $key => $result){
			if(is_null($result['id'])){
				
				
				$iw['content']="<div class='iwContent'><h4>Nest Location</h4>Unknown Pokemon Type<hr><button class='btn btn-warning ' onclick='suggestModal({$result['nest_id']})'>Suggest Pokemon</button></div>";
				$result['infoWindow']=$iw;
				$result['opacity']=0.8;
				$result['sx']=30;
				$result['sy']=30;
				$result['ax']=15;
				$result['ay']=15;
				$result['ox']=0;
				$result['oy']=0;
				$result['icon']['url'] = "assets/img/map/icons/questionmark.png";
			} else {
				$iw['content']="<div class='iwContent'><h3>{$result['name']} Nest</h3></div>";
				$result['infoWindow']=$iw;
				$result['sx']=80;
				$result['sy']=80;
				$result['ax']=40;
				$result['ay']=40;
				$result['ox']=0;
				$result['oy']=0;
				$result['icon']['url'] = $result['id'];
				for ($x = 0; strlen($result['icon']['url']) < 3; $x++) {
					$result['icon']['url'] = "0".$result['icon']['url'];
				}
				$result['icon']['url']= "assets/img/map/pokemon/png/".$result['icon']['url']."-1.png";
			}
			$locations['nests'][] = $result;
		}
	}
	$content = json_encode($locations);
	$length = strlen($content);

	header('Content-Length: '.$length);
	echo json_encode($locations);
}
?>